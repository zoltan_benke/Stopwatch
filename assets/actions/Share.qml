import bb.cascades 1.2

ActionItem{
    id: root
    signal clicked
    
    property bool freeVersion: app.freeVersion
    
    enabled: !freeVersion
    title: qsTr("Share").concat(freeVersion ? " (PRO)" : "") + Retranslate.onLocaleOrLanguageChanged
    imageSource: "asset:///images/actions/ic_share.png"
    onTriggered: {
        root.clicked()
    }   
}