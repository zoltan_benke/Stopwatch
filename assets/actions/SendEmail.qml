import bb.cascades 1.2

ActionItem{
    id: root
    signal clicked
    
    title: qsTr("Send e-mail") + Retranslate.onLocaleOrLanguageChanged
    imageSource: "asset:///images/actions/ic_email.png"
    onTriggered: {
        root.clicked()
    }   
}