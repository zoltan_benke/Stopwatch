import bb.cascades 1.2

ActionItem {
    id: root
    
    signal clicked
    
    title: qsTr("Set as default") + Retranslate.onLocaleOrLanguageChanged
    imageSource: root.enabled ? "asset:///images/actions/ic_set_as_default.png" :   "asset:///images/actions/ic_set_as_default_dk.png" 
    onTriggered: {
        root.clicked()
    }   
}