import bb.cascades 1.2

ActionItem{
    id: root
    signal clicked
    
    property bool freeVersion: app.freeVersion
    
    enabled: !freeVersion
    title: qsTr("Save to file").concat(freeVersion ? " (PRO)" : "") + Retranslate.onLocaleOrLanguageChanged
    imageSource: "asset:///images/actions/ic_save_as.png"
    onTriggered: {
        root.clicked()
    }   
}