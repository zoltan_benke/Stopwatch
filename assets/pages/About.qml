import bb.cascades 1.2
import bb 1.0
import "../components"

SourcePage {
    id: root
    
    destroy: true
    header: TopHeader {
        title: aboutAction.title
    }
    
    onActiveChanged: {
        if (active){
            console.log("onActiveChanged, About.qml, false")
            aboutAction.enabled = false
        }else{
            aboutAction.enabled = true
            console.log("onActiveChanged, About.qml, true")
        }
    }
    
    attachedObjects: [
        ApplicationInfo {
            id: info
        },
        Invoker {
            id: invoker
        }
    ]
    
    ImageView {
        horizontalAlignment: HorizontalAlignment.Right
        verticalAlignment: VerticalAlignment.Bottom
        preferredHeight: 500
        scalingMethod: ScalingMethod.AspectFit
        imageSource: "asset:///images/devpda.png"
        opacity: 0.5
    }
    
    
    ScrollView {
        scrollRole: ScrollRole.Main
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        scrollViewProperties.pinchToZoomEnabled: false
        
        
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            topPadding: 20
            
            ImageView {
                horizontalAlignment: HorizontalAlignment.Center
                preferredHeight: 250
                scalingMethod: ScalingMethod.AspectFit
                imageSource: "asset:///images/icon.png"
                bottomMargin: 0
            }
            
            MyLabel {
                topMargin: 5
                horizontalAlignment: HorizontalAlignment.Center
                size: 15
                text: info.title
                bottomMargin: 0
            }
            
            MyLabel {
                topMargin: 0
                horizontalAlignment: HorizontalAlignment.Center
                size: 7
                text: qsTr("Version: %1").arg(info.version) + Retranslate.onLocaleOrLanguageChanged
                bottomMargin: 2
            }

            Header {
                title: qsTr("Developer") + Retranslate.onLocaleOrLanguageChanged
            }
            
            MyLabel {
                horizontalAlignment: HorizontalAlignment.Center
                size: 8
                text: "Zoltán Benke"
                bottomMargin: 0
            }
            MyLabel {
                topMargin: 5
                horizontalAlignment: HorizontalAlignment.Center
                size: 8
                text: "http://devpda.net"
                color: Color.create("#".concat(settings.color))
                onTouch: {
                    invoker.target = "sys.browser"
                    invoker.action = "bb.action.OPEN"
                    invoker.uri = text.trim()
                    invoker.invoke()
                }
            }
            MyLabel {
                multiline: true
                horizontalAlignment: HorizontalAlignment.Fill
                size: 8
                align: TextAlign.Center
                text: qsTr("In case of any problems, please contact me") + Retranslate.onLocaleOrLanguageChanged
                bottomMargin: 0
            }
            Button {
                horizontalAlignment: HorizontalAlignment.Center
                maxHeight: 70
                maxWidth: 70
                imageSource: app.whiteTheme ? "asset:///images/contact_me_black.png" : "asset:///images/contact_me.png"
                onClicked: {
                    invoker.target = "sys.pim.uib.email.hybridcomposer"
                    invoker.action = "bb.action.SENDEMAIL"
                    invoker.uri = "mailto:support@devpda.net?subject="+info.title+" (BlackBerry)"
                    invoker.invoke()
                }
            }       
        }
    }

}