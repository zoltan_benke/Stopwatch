import bb.cascades 1.2
import bb.system 1.2

import "../components"

SourcePage {
    id: root
    
    destroy: true
    header: TopHeader {
        id: topHeader
        title: settingsAction.title
    }
    
    onActiveChanged: {
        if (active){
            settingsAction.enabled = false
        }else{
            settingsAction.enabled = true
        }
    }
    
    
    onCreationCompleted: {
        appTheme.themeSaved.connect(toast.show)
        var theme = appTheme.getThemeString("theme")
        console.log("THEME: ".concat(theme))
        themeDropDown.setSelectedIndex(theme == "bright" ? 0 : 1)
        frameDropDown.setSelectedIndex(settings.frameBattery ? 0 : 1)
    }
    
    
    attachedObjects: [
        SystemToast {
            id: toast
            body: qsTr("To apply changes restart application") + Retranslate.onLocaleOrLanguageChanged
            button.label: ""
            autoUpdateEnabled: true
        }
    ]
    
    ScrollView {
        scrollRole: ScrollRole.Main
        scrollViewProperties.pinchToZoomEnabled: false
        scrollViewProperties.scrollMode: ScrollMode.Vertical
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Fill
            
            Header {
                title: qsTr("General settings") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                topPadding: 10
                leftPadding: 10
                rightPadding: 10
                bottomPadding: 10
                
                Container {
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: 1
                    }
                    Label {
                        horizontalAlignment: HorizontalAlignment.Left
                        text: qsTr("Show customize dialog")  + Retranslate.onLocaleOrLanguageChanged
                        bottomMargin: 0
                    }
                    Label {
                        multiline: true
                        topMargin: 0
                        text: qsTr("Shows customize dialog after you add stopwatch") + Retranslate.onLocaleOrLanguageChanged
                        textStyle{
                            fontSize: FontSize.PointValue
                            fontSizeValue: 5
                            color: Color.Gray
                        }
                    }
                }
                ToggleButton {
                    horizontalAlignment: horizontalAlignment.Right
                    verticalAlignment: VerticalAlignment.Center
                    checked: settings.showCustomizeDialog
                    layoutProperties: StackLayoutProperties {
                        spaceQuota: -1
                    }
                    onCheckedChanged: {
                        settings.showCustomizeDialog = checked
                    }
                }
            }
            
            Header {
                title: qsTr("Active frame") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                DropDown {
                    id: frameDropDown
                    
                    title: qsTr("Frame") + Retranslate.onLocaleOrLanguageChanged
                    
                    Option {
                        text: qsTr("Battery status") + Retranslate.onLocaleOrLanguageChanged
                        value: true
                    }
                    
                    Option {
                        text: qsTr("Active stopwatch") + Retranslate.onLocaleOrLanguageChanged
                        value: false
                    }
                    onSelectedIndexChanged: {
                        settings.frameBattery = selectedValue
                    }
                }
            } // end of dropdown
            
            Header {
                title: qsTr("Application theme") + Retranslate.onLocaleOrLanguageChanged
            }
            
            Container {
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                DropDown {
                    id: themeDropDown
                    
                    title: qsTr("Theme") + Retranslate.onLocaleOrLanguageChanged
                    
                    Option {
                        text: qsTr("Bright") + Retranslate.onLocaleOrLanguageChanged
                        value: VisualStyle.Bright
                    }
                    
                    Option {
                        text: qsTr("Dark") + Retranslate.onLocaleOrLanguageChanged
                        value: VisualStyle.Dark
                    }
                    onSelectedIndexChanged: {
                        appTheme.setThemeString("theme", selectedOption.value === VisualStyle.Bright ? "bright" : "dark")
                    }
                }
            } // end of dropdown
            
            Header {
                visible: !app.newOs
                title: qsTr("Application color") + Retranslate.onLocaleOrLanguageChanged
            }
            
            ListView {
                visible: !app.newOs
                leftPadding: 10
                rightPadding: 10
                topPadding: 5
                layout: GridListLayout {
                    horizontalCellSpacing: 10
                    verticalCellSpacing: 10
                    orientation: LayoutOrientation.TopToBottom
                    columnCount: 4
                }
                preferredHeight: Qt.sizeHelper.nType ? 175 : 186
                onCreationCompleted: {
                    var items = ["0098f0", "96b800", "cc3f10", "a30d7e"]
                    model.append(items)
                
                }
                dataModel: ArrayDataModel {
                    id: model
                }
                listItemComponents: [
                    ListItemComponent {
                        type: ""
                        ImageView {
                            scaleX: Qt.settings.color === ListItemData ? 0.8 : 1
                            scaleY: scaleX
                            preferredHeight: 150
                            scalingMethod: ScalingMethod.AspectFit
                            imageSource: "asset:///images/colors/".concat(ListItemData).concat(".png")
                        }
                    }
                ]
                onTriggered: {
                    var item = dataModel.data(indexPath)
                    if (item){
                        settings.color = item
                    }
                }
                scrollIndicatorMode: ScrollIndicatorMode.None
            } // end of ListView
        
        
        } // end of RootContainer
    }

}