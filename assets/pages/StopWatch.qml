import bb.cascades 1.2

import "../components"
import "../items"
import "../actions"

SourcePage {
    id: root
    
    header: TopHeader {
        title: app.currentItem.name
    }
    
    property int orientation: sizeHelper.orientation
    attachedObjects: [
        ComponentDefinition {
            id: lapEditDialog
            EditDialog {
            
            }
        },
        ComponentDefinition {
            id: customizeDialog
            CustomizeDialog {
            
            }
        }
    ]
    
    onCreationCompleted: {
        model.append(app.currentItem.laps)
    }
    
    actions: [
        ActionItem {
            ActionBar.placement: ActionBarPlacement.OnBar
            title: app.currentItem.running ? qsTr("Stop") + Retranslate.onLocaleOrLanguageChanged : qsTr("Start") + Retranslate.onLocaleOrLanguageChanged
            imageSource: app.currentItem.running ? "asset:///images/actions/ic_stop.png" : "asset:///images/actions/ic_start.png"
            onTriggered: {
                if (app.currentItem.running){
                    app.currentItem.stop()
                }else{
                    app.currentItem.start()
                }
            }
        },
        ActionItem {
            ActionBar.placement: ActionBarPlacement.OnBar
            title: app.currentItem.running ? qsTr("Lap") + Retranslate.onLocaleOrLanguageChanged : qsTr("Reset") + Retranslate.onLocaleOrLanguageChanged
            imageSource: app.currentItem.running ? "asset:///images/actions/ic_add.png" : "asset:///images/actions/ic_delete.png" + Retranslate.onLocaleOrLanguageChanged
            onTriggered: {
                if (app.currentItem.running){
                    app.currentItem.lap()
                    model.append(app.currentItem.latestLap)
                    listView.scrollToItem([model.size()-1], ScrollAnimation.Smooth)
                }else{
                    app.currentItem.reset()
                    model.clear()
                }
            }
        },
        Export {
            ActionBar.placement: ActionBarPlacement.OnBar
            onClicked: {
                Export.iniciate()
            }
        },
        ShareTxt {
            ActionBar.placement: ActionBarPlacement.OnBar
            onClicked:{
                Export.result = ""
                Export.exportItemToTxt(app.currentItem, true)
                var name = app.currentItem.name.concat("_(").concat(Qt.formatDateTime(new Date(), "MM.dd.yyyy hh:mm")).concat(").txt")
                Export.shareFile(name)
            }
        },
        ShareCsv {
            ActionBar.placement: ActionBarPlacement.OnBar
            onClicked:{
                Export.result = ""
                Export.exportItemToTxt(app.currentItem, true)
                var name = app.currentItem.name.concat("_(").concat(Qt.formatDateTime(new Date(), "MM.dd.yyyy hh:mm")).concat(").txt")
                Qt.Export.shareFile(name)
            }    
        },
        CustomizeAction {
            ActionBar.placement: ActionBarPlacement.OnBar
            onTriggered: {
                var dialog = customizeDialog.createObject()
                dialog.item = app.currentItem
                dialog.open()
            }
        }
    ]
    
    
    Container {
        id: rootContainer
        layout: StackLayout {
            orientation: !root.orientation ? LayoutOrientation.TopToBottom : LayoutOrientation.LeftToRight
        }
        
        Container {
            preferredWidth: !root.orientation ? sizeHelper.maxWidth : sizeHelper.maxWidth/2
            
            Container {
                layout: StackLayout {}
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                preferredHeight: root.orientation ? sizeHelper.maxHeight/2 : undefined
                MyLabel {
                    horizontalAlignment: HorizontalAlignment.Center
                    
                    text: app.currentItem.edited ? app.currentItem.eTitle : app.currentItem.title
                    color: Color.create(app.currentItem.colorAccent)
                    size: !root.orientation ? 32 : 28
                } // end of TitleLabel
                MyLabel {
                    visible: app.currentItem.edited
                    topMargin: 0
                    horizontalAlignment: HorizontalAlignment.Fill
                    align: TextAlign.Center
                    text: app.currentItem.title
                    color: Color.create(app.currentItem.colorAccent)
                    size: 8
                } // end of TitleLabel
                bottomMargin: 0
                topMargin: 0
            }
            
            LapStatsHeader {
            
            }
            
            LapStatisticsPortrait {
                visible: !root.orientation
            }
            
            LapStatisticsLandscape {
                preferredHeight: sizeHelper.maxHeight/2
                visible: root.orientation
            }
            
            Divider {
                visible: !root.orientation
                topMargin: 0
            }
        } // end of Top container
        
        // --------------------------------------------------------- //
        
        ListView {
            id: listView
            visible: app.currentItem.totalLaps
            preferredWidth: !root.orientation ? sizeHelper.maxWidth : sizeHelper.maxWidth/2
            verticalAlignment: VerticalAlignment.Fill
            dataModel: ArrayDataModel {
                id: model
            }
            
            function removeLap(indexPath){
                var item = dataModel.data(indexPath)
                if (item){
                    var check = app.currentItem.removeLap(item)
                    console.log("CHECK "+check)
                    if (check)
                        dataModel.removeAt(indexPath)
                }
            }
            
            function edit(indexPath){
                clearSelection()
                select(indexPath)
                var item = dataModel.data(indexPath)
                if (item){
                    var dialog = lapEditDialog.createObject()
                    dialog.setLap(item)
                    dialog.saved.connect(updateLap)
                    dialog.open()
                }
            }
            
            function updateLap(newLap){
                var indexPath = selected();
                if (newLap){
                    model.replace(indexPath, newLap)
                    model.itemUpdated(indexPath)
                }
            }
            
            listItemComponents: [
                ListItemComponent {
                    type: ""
                    ListItemLaps{}
                }
            ] 
        } // end of listview
        
        
        Container {
            visible: !app.currentItem.totalLaps
            preferredWidth: !root.orientation ? sizeHelper.maxWidth : sizeHelper.maxWidth/2
            horizontalAlignment: HorizontalAlignment.Fill
            MyLabel {
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                
                opacity: 0.5
                text: qsTr("No laps") + Retranslate.onLocaleOrLanguageChanged
                size: 20
                color: Color.Gray
            }
        } // end of No Laps
    }
}