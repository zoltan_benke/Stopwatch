import bb.cascades 1.2
import bb.cascades.pickers 1.0

import "../components"
import "../actions"

SourcePage {
    id: root
    
    
    function doSave(share){
        fileName = (exportStopwatch ? app.currentItem.name.trim() : "stopwatch") + "_(" + Qt.formatDateTime(new Date(), filenameFormat.selectedValue)+")."+(fileformat.selectedOption ? fileformat.selectedOption.text : "")
        Export.result = ""
        if (exportStopwatch){ // EXPORT ONE ITEM
            if (fileformat.selectedValue === 0){ // TXT FORMAT
                Export.exportItemToTxt(app.currentItem)
            }else{ // CSV FORMAT
                Export.exportItemToCsv(app.currentItem, true, csvDelimeter.selectedValue)
            }
        }else{ // Export all
            if (fileformat.selectedValue === 0){ // TXT FORMAT
                Export.exportAllToTxt(app.sourceModel)
            }else{ // CSV FORMAT
                Export.exportAllToCsv(app.sourceModel, true, csvDelimeter.selectedValue)
            }
        }
        if (share){
            Export.shareFile(fileName)
        }else{
            Export.saveToFile(fileName)
        }
    }
    
    
    function sendEmail(){
        fileName = (exportStopwatch ? app.currentItem.name.trim() : "stopwatch") + "_(" + Qt.formatDateTime(new Date(), filenameFormat.selectedValue)+")."+(fileformat.selectedOption ? fileformat.selectedOption.text : "")
        Export.result = ""
        if (exportStopwatch){ // EXPORT ONE ITEM
            Export.exportItemToTxt(app.currentItem)
        }else{ // Export all
            Export.exportAllToTxt(app.sourceModel)
        }
        Export.invokeEmail(fileName, false)
    }
    
    
    property string fileName
    property bool exportStopwatch
    
    onFileNameChanged: {
        console.log("onFileNameChanged, "+fileName)
    }
    destroy: true
    header: TopHeader {
        title: qsTr("Export") + Retranslate.onLocaleOrLanguageChanged
    }
    
    
    actions: [
        Share {
            ActionBar.placement: ActionBarPlacement.OnBar
            onClicked: {
                doSave(true)
            }
        },
        SaveToFile {
            ActionBar.placement: ActionBarPlacement.OnBar
            onTriggered: {
                doSave(false)
            }
        },
        SendEmail {
            enabled: (fileformat.selectedValue === 0)
            ActionBar.placement: ActionBarPlacement.OnBar
            onTriggered: {
                sendEmail()
            }
        }
    ]
    paneProperties: NavigationPaneProperties {
        backButton: ActionItem {
            title: qsTr("Cancel") + Retranslate.onLocaleOrLanguageChanged
            onTriggered: {
                activePane.pop()
            }
        }
    }
    ScrollView {
        accessibility.name: "Content"
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        scrollViewProperties{
            pinchToZoomEnabled: false
            scrollMode: ScrollMode.Vertical
        }
        
        Container {
            topPadding: sizeHelper.margin
            leftPadding: sizeHelper.margin
            rightPadding: sizeHelper.margin
            
            DropDown {
                id: fileformat
                title: qsTr("File format") + Retranslate.onLocaleOrLanguageChanged
                selectedIndex: settings.fileFormat
                Option {
                    text: "txt"
                    value: 0
                }
                
                Option {
                    text: "csv"
                    value: 1
                }
                onSelectedIndexChanged: {
                    settings.fileFormat = selectedIndex
                }
            } // end of File format dropdown
            
            DropDown {
                id: csvDelimeter
                visible: fileformat.selectedValue === 1
                title: qsTr("CSV delimiter") + Retranslate.onLocaleOrLanguageChanged
                selectedIndex: settings.csvDelimeter
                Option {
                    text: qsTr("tab") + Retranslate.onLocaleOrLanguageChanged
                    value: "\t"
                }
                Option {
                    text: qsTr("comma") + Retranslate.onLocaleOrLanguageChanged
                    value: ","
                }
                Option {
                    text: qsTr("semicolon") + Retranslate.onLocaleOrLanguageChanged
                    value: ";"
                }
                onSelectedIndexChanged: {
                    settings.csvDelimeter = selectedIndex
                }
            } // end of CVSdelimeter
            
            DropDown {
                id: filenameFormat
                title: qsTr("Filename format") + Retranslate.onLocaleOrLanguageChanged
                selectedIndex: settings.fileNameFormat
                Option {
                    text: (root.exportStopwatch ? app.currentItem.name.trim() : "stopwatch") +"_("+qsTr("date-time")+")."+(fileformat.selectedOption ? fileformat.selectedOption.text : "")
                    value: "MM.dd.yyyy hh:mm"
                }
                Option {
                    text: (root.exportStopwatch ? app.currentItem.name.trim() : "stopwatch") + "_("+qsTr("date")+")."+(fileformat.selectedOption ? fileformat.selectedOption.text : "")
                    value: "MM.dd.yyyy"
                }
                Option {
                    text: (root.exportStopwatch ? app.currentItem.name.trim() : "stopwatch") + "_("+qsTr("time")+")."+(fileformat.selectedOption ? fileformat.selectedOption.text : "")
                    value: "hh:mm"
                }
                onSelectedIndexChanged: {
                    settings.fileNameFormat = selectedIndex
                }
            }
            
            DropDown {
                id: timeFormat
                title: qsTr("Export time format") + Retranslate.onLocaleOrLanguageChanged
                selectedIndex: settings.timeFormat
                Option {
                    text: "00:00:000 (mm:ss:zzz)"
                    value: "mm:ss:zzz"
                }
                Option {
                    text: "0:0:0 (m:s:z)"
                    value: "m:s:z"
                }
                Option {
                    text: qsTr("Custom format") + Retranslate.onLocaleOrLanguageChanged
                }
                onSelectedOptionChanged: {
                    if (selectedIndex !== 2){
                        Export.timeFormat = selectedValue
                    }
                    settings.timeFormat = selectedIndex
                }
            } // end of TimeFormat
            
            Container {
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                visible: (timeFormat.selectedIndex === 2)
                TextField {
                    id: timeFormatField
                    hintText: qsTr("Enter custom time format") + Retranslate.onLocaleOrLanguageChanged
                    inputMode: TextFieldInputMode.Text
                    textFormat: TextFormat.Plain
                    text: settings.timeFormatString
                    input{
                        submitKeyFocusBehavior: SubmitKeyFocusBehavior.Lose
                        submitKey: SubmitKey.EnterKey
                        onSubmitted: {
                            Export.timeFormat = text
                            settings.timeFormatString = text
                        }
                    }
                    onFocusedChanged: {
                        if (!focused) {Export.timeFormat = text; settings.timeFormatString = text}
                    }
                    onTextChanging: {
                        timeFormatField.text = text.toLowerCase().trim()
                    }
                } // end of Custom time format
                
                Button {
                    preferredHeight: 50
                    preferredWidth: 50
                    text: "?"
                    onClicked: {
                        toast.button.label = "Ok"
                        toast.body = qsTr("Time format\n\nm - (0 to 59)\nmm - (00 to 59)\ns - (0 to 59)\nss - (00 to 59)\nz - (0 to 999)\nzzz - (000 to 999)\n\nexample:\nm,ss-zzz\noutput:\n2,59-987") + Retranslate.onLocaleOrLanguageChanged
                        toast.show()
                    }
                }
            } // end of Custom TimeFormat Container
            
            RadioGroup {
                selectedIndex: root.exportStopwatch ? 0 : 1
                Option {
                    text: qsTr("Active stopwatch") + Retranslate.onLocaleOrLanguageChanged
                    value: true
                }
                Option {
                    text: qsTr("All stopwatch") + Retranslate.onLocaleOrLanguageChanged
                    value: false
                }
                onSelectedOptionChanged: {
                    root.exportStopwatch = selectedValue
                }
            }
        
        } // end of Root Container
    } // end of ScrollView
}