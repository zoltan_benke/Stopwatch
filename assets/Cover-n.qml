import bb.cascades 1.2
import bb.device 1.2
import "components"


Container {
    id: root
    layout: DockLayout {}
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    background: app.whiteTheme ? Color.White : Color.Black
    
    attachedObjects: [
        BatteryInfo {
            id: battery
            onLevelChanged: {
                batterLevelLabel.text = level+"%"
                batteryStatusLabel.text = getBatteryStatus(newChargingState)
            }
            onFullChargeCapacityChanged: {
                console.log("FULL CHARGE CAPACITY "+fullChargeCapacity)
            }
        }
    ]
    
    function getBatteryStatus(state){
        switch (state){
            case BatteryChargingState.Unknown:
                return qsTr("Unknown") + Retranslate.onLocaleOrLanguageChanged
            case BatteryChargingState.NotCharging:
                return qsTr("Not charging") + Retranslate.onLocaleOrLanguageChanged
            case BatteryChargingState.Charging:
                return qsTr("Charging") + Retranslate.onLocaleOrLanguageChanged
            case BatteryChargingState.Discharging:
                return qsTr("Discharging") + Retranslate.onLocaleOrLanguageChanged
            case BatteryChargingState.Full:
                return qsTr("Full") + Retranslate.onLocaleOrLanguageChanged
            default:
                break;
        }
    }
    
    
    Container {
        preferredHeight: 40
        layout: DockLayout{}
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Top
        background: app.whiteTheme ? Color.create("#f5f5f5") : Color.create("#262626")
        MyLabel {
            verticalAlignment: VerticalAlignment.Center
            horizontalAlignment: HorizontalAlignment.Center
            text: !settings.frameBattery ? app.currentItem.name ? app.currentItem.name : qsTr("Active stopwatch") + Retranslate.onLocaleOrLanguageChanged : qsTr("Battery status") + Retranslate.onLocaleOrLanguageChanged
            family: "Slate Pro Font"
            color: app.whiteTheme ? Color.Black : Color.White
            size: 7
        }
        Container {
            horizontalAlignment: HorizontalAlignment.Fill
            verticalAlignment: VerticalAlignment.Bottom
            //visible: !app.newOs
            layout: DockLayout{}
            preferredHeight: 3
            background: Color.create("#".concat(settings.color))
        }
    }
    
    Container {
        topPadding: 40
        visible: !settings.frameBattery
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Center
        leftPadding: 10
        rightPadding: 10
        MyLabel {
            horizontalAlignment: HorizontalAlignment.Center
            family: "Slate Pro Font"
            size: 13
            text: app.currentItem.edited ? app.currentItem.eTitle : app.currentItem.title
            color: Color.create("#".concat(app.currentItem.colorAccent))
        }
        Divider{topMargin: 0; bottomMargin: 0}
        RowLabel {
            topMargin: 0
            bottomMargin: 0
            horizontalAlignment: HorizontalAlignment.Fill
            fontSizeTitle: 4
            fontSizeValue: 4
            valueColor: app.whiteTheme ? Color.Black : Color.White
            title: qsTr("Best lap") + Retranslate.onLocaleOrLanguageChanged + ": "
            value: Qt.formatTime(new Date(app.currentItem.bestLap), "mm:ss:zzz") 
        }
        RowLabel {
            topMargin: 0
            bottomMargin: 0
            fontSizeTitle: 4
            fontSizeValue: 4
            valueColor: app.whiteTheme ? Color.Black : Color.White
            title: qsTr("Worst lap") + Retranslate.onLocaleOrLanguageChanged + ": "
            value: Qt.formatTime(new Date(app.currentItem.worstLap), "mm:ss:zzz")
        }
        RowLabel {
            topMargin: 0
            bottomMargin: 0
            fontSizeTitle: 4
            fontSizeValue: 4
            valueColor: app.whiteTheme ? Color.Black : Color.White
            title: qsTr("Average lap") + Retranslate.onLocaleOrLanguageChanged + ": "
            value: Qt.formatTime(new Date(app.currentItem.averageLap), "mm:ss:zzz")
        }
        RowLabel {
            topMargin: 0
            bottomMargin: 0
            fontSizeTitle: 4
            fontSizeValue: 4
            valueColor: app.whiteTheme ? Color.Black : Color.White
            title: qsTr("Latest lap") + Retranslate.onLocaleOrLanguageChanged + ": "
            value:  app.currentItem.latestLap.lap
        }
    }
    
    Container {
        visible: settings.frameBattery
        topPadding: 40
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        MyLabel {
            id: batterLevelLabel
            horizontalAlignment: HorizontalAlignment.Center
            family: "Slate Pro Font"
            size: 20
            text: battery.level+"%"
            color: Color.create("#".concat(settings.color))
            bottomMargin: 0
        }
        MyLabel {
            topMargin: 0
            id: batteryStatusLabel
            horizontalAlignment: HorizontalAlignment.Center
            size: 6
            family: "Slate Pro Font"
            text: getBatteryStatus(battery.chargingState)
            color: app.whiteTheme ? Color.Black : Color.White
        }
    }

}