/*
 * Copyright (c) 2011-2014 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import bb.cascades 1.2
import bb.device 1.2
import bb.system 1.2
import "tabs"
import "../components"
import "pages"

TabbedPane {
    id: rootPane
    
    
    function log(status, msg){
        console.log(status+" | "+msg)
    }
    
    
    function onSave(activeStopwatch){
        var page = savePage.createObject()
        page.exportStopwatch = activeStopwatch
        activePane.push(page)
    }
    
    
    Menu.definition: MenuDefinition {
        settingsAction: SettingsActionItem {
            id: settingsAction
            title: qsTr("Settings") + Retranslate.onLocaleOrLanguageChanged
            onTriggered: {
                var settings = settingsPage.createObject()
                activePane.push(settings)
            }
        }
        helpAction: HelpActionItem {
            id: aboutAction
            title: qsTr("About") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///images/actions/ic_info.png"
            onTriggered: {
                var about = aboutPage.createObject()
                activePane.push(about)
            }
        }
        actions: [
            ActionItem {
                title: qsTr("Rate") + Retranslate.onLocaleOrLanguageChanged
                imageSource: "asset:///images/actions/ic_rate.png"
                onTriggered: {
                    invoker.action = "bb.action.OPEN"
                    invoker.target = "sys.appworld"
                    invoker.uri = "appworld://content/SKU59935315"
                    invoker.invoke()
                }
            },
            ActionItem {
                title: qsTr("Tell a Friend") + Retranslate.onLocaleOrLanguageChanged
                imageSource: "asset:///images/actions/ic_invite.png"
                onTriggered: {
                    if (BBM.allowed)
                        Invite.sendInvite();
                    else
                        BBM.registerApplication()
                }
            }
        ]
    }
    
    attachedObjects: [
        ComponentDefinition {
            id: settingsPage
            Settings{}
        },
        ComponentDefinition {
            id: aboutPage
            About {
            
            }
        },
        ComponentDefinition {
            id: savePage
            source: "pages/SaveFile.qml"
        },
        Invoker {
            id: invoker
        },
        SystemToast {
            id: toast
            button.label: ""
            onFinished: {
                if (value == SystemUiResult.ButtonSelection){
                    cancel()
                }
            }
        }
    ]
    
    Tab {
        NaviPage {
            StopWatches {
            
            }
        }
        
    }
    
    
    
    onCreationCompleted: {
        Export.start.connect(onSave)
        if (!BBM.allowed)
            BBM.registerApplication()
        Qt.app = app;
        Qt.sourceModel = sourceModel
        Qt.settings = settings
        Qt.sizeHelper = sizeHelper
        Qt.Export = Export
    }
}
