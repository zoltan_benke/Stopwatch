import bb.cascades 1.2


Container {
    id: statsContainer
    layout: StackLayout {
        orientation: LayoutOrientation.LeftToRight
    }
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        StatsContainer {
            topPadding: 5
            bottomPadding: 5
            leftPadding: 5
            rightPadding: 5
            minWidth: (Qt.sizeHelper.maxWidth) / 4
            maxWidth: (Qt.sizeHelper.maxWidth) / 4
            
            key: qsTr("Best lap") + Retranslate.onLocaleOrLanguageChanged
            keySize: 6
            keyBold: FontWeight.Bold
            
            value: Qt.app.currentItem ? Qt.formatTime(new Date(Qt.app.currentItem.bestLap), "mm:ss:zzz") : ""
            valueSize: 7
        } // end of BestLap
        
        Container {
            background: Qt.app.whiteTheme ? Color.Black : Color.LightGray
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Left
            maxHeight: Infinity                        
            minWidth: 1
            maxWidth: 1
        }
        
        StatsContainer {
            topPadding: 5
            bottomPadding: 5
            leftPadding: 5
            rightPadding: 5
            minWidth: (Qt.sizeHelper.maxWidth) / 4
            maxWidth: (Qt.sizeHelper.maxWidth) / 4
            
            key: qsTr("Worst lap") + Retranslate.onLocaleOrLanguageChanged
            keySize: 6
            keyBold: FontWeight.Bold
            
            value: Qt.app.currentItem ? Qt.formatTime(new Date(Qt.app.currentItem.worstLap), "mm:ss:zzz") : ""
            valueSize: 7

        } // end of WorstLap
        
        
        Container {
            background: Qt.app.whiteTheme ? Color.Black : Color.LightGray
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Left
            maxHeight: Infinity                        
            minWidth: 1
            maxWidth: 1
        }
        
        StatsContainer {
            topPadding: 5
            bottomPadding: 5
            leftPadding: 5
            rightPadding: 5
            minWidth: (Qt.sizeHelper.maxWidth) / 4
            maxWidth: (Qt.sizeHelper.maxWidth) / 4
            
            key: qsTr("Average lap") + Retranslate.onLocaleOrLanguageChanged
            keySize: 6
            keyBold: FontWeight.Bold
            
            value: Qt.app.currentItem ? Qt.formatTime(new Date(Qt.app.currentItem.averageLap), "mm:ss:zzz") : ""
            valueSize: 7
        
        } // end of AverageLap
        
        Container {
            background: Qt.app.whiteTheme ? Color.Black : Color.LightGray
            verticalAlignment: VerticalAlignment.Fill
            horizontalAlignment: HorizontalAlignment.Left
            maxHeight: Infinity                        
            minWidth: 1
            maxWidth: 1
        }
        
        StatsContainer {
            topPadding: 5
            bottomPadding: 5
            leftPadding: 5
            rightPadding: 5
            minWidth: (Qt.sizeHelper.maxWidth) / 4
            maxWidth: (Qt.sizeHelper.maxWidth) / 4
            
            key: qsTr("Latest lap") + Retranslate.onLocaleOrLanguageChanged
            keySize: 6
            keyBold: FontWeight.Bold
            
            value: Qt.app.currentItem ? Qt.app.currentItem.latestLap.lap : ""
            valueSize: 7
        
        } // end of latestLap
    }
} // end of statsContainer