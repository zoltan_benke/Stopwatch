import bb.cascades 1.2

import "../components"
import "../items"
import "../actions"

SourcePage {
    id: root
    
    header: TopHeader {
        id: topHeader
        dotsVisible: true
        title: app.currentItem.name
    }
    
    attachedObjects: [
        ComponentDefinition {
            id: customizeDialog
            CustomizeDialog {
            
            }
        }
    ]
    
    onCreationCompleted: {
        //model.append(app.currentItem.laps)
        pageModel.append([0, 1])
    }
    
    actions: [
        ActionItem {
            ActionBar.placement: ActionBarPlacement.OnBar
            title: app.currentItem.running ? qsTr("Stop") + Retranslate.onLocaleOrLanguageChanged : qsTr("Start") + Retranslate.onLocaleOrLanguageChanged
            imageSource: app.currentItem.running ? "asset:///images/actions/ic_stop.png" : "asset:///images/actions/ic_start.png"
            onTriggered: {
                if (app.currentItem.running){
                    app.currentItem.stop()
                }else{
                    app.currentItem.start()
                }
            }
            shortcuts: Shortcut {
                key: "S"
            }
        },
        ActionItem {
            ActionBar.placement: ActionBarPlacement.OnBar
            title: app.currentItem.running ? qsTr("Lap") + Retranslate.onLocaleOrLanguageChanged : qsTr("Reset") + Retranslate.onLocaleOrLanguageChanged
            imageSource: app.currentItem.running ? "asset:///images/actions/ic_add.png" : "asset:///images/actions/ic_delete.png" + Retranslate.onLocaleOrLanguageChanged
            onTriggered: {
                if (app.currentItem.running){
                    app.currentItem.lap()
                }else{
                    app.currentItem.reset()
                }
            }
            shortcuts: Shortcut {
                key: app.currentItem.running ? "L" : "R" 
            }
        },
        Export {
            ActionBar.placement: ActionBarPlacement.OnBar
            onClicked: {
                Export.iniciate()
            }
            shortcuts: Shortcut {
                key: "E"
            }
        },
        ShareTxt {
            ActionBar.placement: ActionBarPlacement.OnBar
            onClicked:{
                Export.result = ""
                Export.exportItemToTxt(app.currentItem, true)
                var name = app.currentItem.name.concat("_(").concat(Qt.formatDateTime(new Date(), "MM.dd.yyyy hh:mm")).concat(").txt")
                Export.shareFile(name)
            }
            shortcuts: Shortcut {
                key: "Alt + T"
            }
        },
        ShareCsv {
            ActionBar.placement: ActionBarPlacement.OnBar
            onClicked:{
                Export.result = ""
                Export.exportItemToTxt(app.currentItem, true)
                var name = app.currentItem.name.concat("_(").concat(Qt.formatDateTime(new Date(), "MM.dd.yyyy hh:mm")).concat(").txt")
                Qt.Export.shareFile(name)
            }
            shortcuts: Shortcut {
                key: "Alt + C"
            }
        },
        CustomizeAction {
            ActionBar.placement: ActionBarPlacement.OnBar
            onTriggered: {
                var dialog = customizeDialog.createObject()
                dialog.item = app.currentItem
                dialog.open()
            }
            shortcuts: Shortcut {
                key: "C"
            }
        }
    ]
    
    
    ListView {
        attachedObjects: [
            ListScrollStateHandler {
                onFirstVisibleItemChanged: {
                    var index = parseInt(firstVisibleItem)
                    topHeader.dotIndex = index
                }
            }
        ]
        scrollIndicatorMode: ScrollIndicatorMode.None
        flickMode: FlickMode.SingleItem
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        dataModel: ArrayDataModel {
            id: pageModel
        }
        
        function itemType(data, indexPath){
            if (indexPath == 0){
                return "stats"
            }else{
                return "laps"
            }
        }
        layout: StackListLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        listItemComponents: [
            ListItemComponent {
                type: "stats"
                Stats {
                    
                }
            },
            ListItemComponent {
                type: "laps"
                Laps {
                }
            }
        ]
    } // end of ListView
}