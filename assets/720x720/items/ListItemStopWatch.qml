import bb.cascades 1.2

/**
 * Q_ENUMS(StopWatchItem::Roles)
 Q_PROPERTY(bool running READ running WRITE setRunning NOTIFY dataChanged)
 Q_PROPERTY(QString title READ title NOTIFY dataChanged)
 Q_PROPERTY(QString time READ time WRITE setTime NOTIFY dataChanged)
 Q_PROPERTY(QString micro READ micro WRITE setMicro NOTIFY dataChanged)
 Q_PROPERTY(bool hours READ hours WRITE setHours NOTIFY dataChanged)
 Q_PROPERTY(QVariantList laps READ laps NOTIFY lapsChanged)
 Q_PROPERTY(int lapsCount READ lapsCount NOTIFY lapsChanged)
 Q_PROPERTY(QVariant latestLap READ latestLap NOTIFY lapsChanged)
 Q_PROPERTY(int bestLap READ bestLap NOTIFY lapsChanged)
 Q_PROPERTY(int worstLap READ worstLap NOTIFY lapsChanged)
 */

import "../components"
import "../actions"

Container {
    id: root
    background: Qt.app.whiteTheme ? Color.create("#f8f8f8") : Color.create("#262626")
    contextActions: [
        ActionSet {
            title: ListItemData.name
            CustomizeAction {
                onClicked: {
                    root.ListItem.view.customize(root.ListItem.indexPath)
                }
            }
            SendEmail {
                onClicked: {
                    Qt.Export.result = ""
                    Qt.Export.exportItemToTxt(ListItemData)
                    var name = ListItemData.name.concat("_(").concat(Qt.formatDateTime(new Date(), "MM.dd.yyyy hh:mm")).concat(").txt")
                    Qt.Export.invokeEmail(name, false)
                }
            }
            ShareTxt {
                freeVersion: Qt.app.freeVersion
                onClicked: {
                    Qt.Export.result = ""
                    Qt.Export.exportItemToTxt(ListItemData)
                    var name = ListItemData.name.concat("_(").concat(Qt.formatDateTime(new Date(), "MM.dd.yyyy hh:mm")).concat(").txt")
                    Qt.Export.shareFile(name)
                }
            }
            ShareCsv {
                freeVersion: Qt.app.freeVersion
                onClicked: {
                    Qt.Export.result = ""
                    Qt.Export.exportItemToCsv(ListItemData, true)
                    var name = ListItemData.name.concat("_(").concat(Qt.formatDateTime(new Date(), "MM.dd.yyyy hh:mm")).concat(").csv")
                    Qt.Export.shareFile(name)
                }
            }
            Export {
                freeVersion: Qt.app.freeVersion
                onClicked: {
                    Qt.Export.iniciate()
                }
            }
        }
    ]
    
    
    layout: DockLayout {}
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    topMargin: 10
    
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Top
        preferredHeight: Qt.app.currentItem == ListItemData ? 2 : 1
        background: Qt.app.currentItem == ListItemData ? Color.create("#".concat(ListItemData.colorAccent)) : Color.LightGray
    }
    Container {
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Bottom
        preferredHeight: Qt.app.currentItem == ListItemData ? 2 : 1
        background: Qt.app.currentItem == ListItemData ? Color.create("#".concat(ListItemData.colorAccent)) : Color.LightGray
    }
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        leftPadding: 15
        rightPadding: 15
        topPadding: 10
        bottomPadding: 10
        ImageButton {
            color: ListItemData.colorAccent
            preferredHeight: 120
            preferredWidth: preferredHeight
            horizontalAlignment: HorizontalAlignment.Right
            verticalAlignment: VerticalAlignment.Center
            text: ListItemData.running ? qsTr("Stop") + Retranslate.onLocaleOrLanguageChanged : (!ListItemData.running && ListItemData.title === "00:00:000") ? qsTr("Remove") + Retranslate.onLocaleOrLanguageChanged : qsTr("Reset") + Retranslate.onLocaleOrLanguageChanged
            textSize: {
                if (ListItemData.running){
                    return 6
                }else{
                    return 5
                }
            }
            onClicked: {
                if (ListItemData.running){
                    Qt.app.stopItem(root.ListItem.indexPath)
                }
                else if (!ListItemData.running && ListItemData.title === "00:00:000"){
                    Qt.app.removeItem(root.ListItem.indexPath)
                }
                else{
                    Qt.app.resetItem(root.ListItem.indexPath)
                }
            }
        }
        
        Container {
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            verticalAlignment: VerticalAlignment.Center
            gestureHandlers: [
                TapHandler {
                    onTapped: {
                        root.ListItem.view.itemClicked(root.ListItem.indexPath)
                    }
                }
            ]
            onTouch: {
                if (event.isDown()){
                    if (Qt.app.currentItem != ListItemData)
                        Qt.app.currentItem = ListItemData
                }
            }
            MyLabel {
                text: ListItemData.name
                horizontalAlignment: HorizontalAlignment.Fill
                align: TextAlign.Center
                size: 5
                color: Qt.app.whiteTheme ? Color.Black : Color.White
                bottomMargin: 0
            }
            MyLabel {
                topMargin: 0
                horizontalAlignment: HorizontalAlignment.Center
                text: ListItemData.edited ? ListItemData.eTitle : ListItemData.title
                color: Qt.app.whiteTheme ? Color.Black : Color.White
                size: 14
                align: TextAlign.Center
                bottomMargin: 0
            } // end of TitleLabel
            
            Container {
                topMargin: 0
                layout: StackLayout {
                    orientation: LayoutOrientation.LeftToRight
                }
                horizontalAlignment: HorizontalAlignment.Center
                MyLabel {
                    text: qsTr("Latest lap") + Retranslate.onLocaleOrLanguageChanged + ": " + ListItemData.latestLap.lap
                    align: TextAlign.Center
                    size: 5
                    color: Qt.app.whiteTheme ? Color.Black : Color.White
                }
                
                MyLabel {
                    visible: ListItemData.totalLaps
                    text: qsTr("Total laps") + Retranslate.onLocaleOrLanguageChanged + ": " + ListItemData.totalLaps
                    align: TextAlign.Center
                    size: 5
                    color: Qt.app.whiteTheme ? Color.Black : Color.White
                }
            } // end of LeftRight container
        } // end of title Container
        
        ImageButton {
            color: ListItemData.colorAccent
            preferredHeight: 120
            preferredWidth: preferredHeight
            horizontalAlignment: HorizontalAlignment.Right
            verticalAlignment: VerticalAlignment.Center
            text: ListItemData.running ? qsTr("Lap") + Retranslate.onLocaleOrLanguageChanged : qsTr("Start") + Retranslate.onLocaleOrLanguageChanged
            textSize: 6
            onClicked: {
                if (ListItemData.running){
                    Qt.app.lapItem(root.ListItem.indexPath)
                }else{
                    Qt.app.startItem(root.ListItem.indexPath)
                }
            }
        }
    }
}