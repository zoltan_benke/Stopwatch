import bb.cascades 1.2

import "../components"
import "../items"
import "../pages"
import "../actions"


SourcePage {
    destroy: true
    
    header: TopHeader {
        title: qsTr("Stopwatch") + Retranslate.onLocaleOrLanguageChanged
    }
    
    actions: [
        Export{
            enabled: app.sourceModelCount && !Qt.app.freeVersion
            ActionBar.placement: ActionBarPlacement.OnBar
            onClicked: {
                Export.iniciate(false)
            }
        },
        ActionItem {
            ActionBar.placement: ActionBarPlacement.OnBar
            title: qsTr("Add") + Retranslate.onLocaleOrLanguageChanged
            imageSource: "asset:///images/actions/ic_add.png"
            onTriggered: {
                app.append()
                if (settings.showCustomizeDialog){
                    var dialog = customizeDialog.createObject()
                    dialog.item = app.sourceModel.data(app.sourceModel.last())
                    dialog.open()
                }
            }
            shortcuts: SystemShortcut {
                type: SystemShortcuts.CreateNew
            }
        }
    ]
    
    attachedObjects: [
        ComponentDefinition {
            id: stopWatch
            source: "../pages/StopWatch.qml"
        },
        ComponentDefinition {
            id: customizeDialog
            CustomizeDialog {
            
            }
        }
    ]
    
    Container {
        visible: !app.sourceModelCount
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        leftPadding: 15
        rightPadding: 15
        MyLabel {
            color: Color.Gray
            align: TextAlign.Center
            multiline: true
            size: 12
            text: qsTr("To add stopwatch use the add button below") + Retranslate.onLocaleOrLanguageChanged
        }
        ImageView {
            horizontalAlignment: HorizontalAlignment.Center
            imageSource: app.whiteTheme ? "asset:///images/actions/ic_add_black.png" : "asset:///images/actions/ic_add.png"
        }
    }
    
    
    ListView {
        id: listView
        topPadding: 15
        visible: app.sourceModelCount
        scrollRole: ScrollRole.Main
        function customize(indexPath){
            var item = dataModel.data(indexPath)
            if (item){
                var dialog = customizeDialog.createObject()
                dialog.item = item
                dialog.open()
            }
        }
        
        function itemClicked(indexPath){
            clearSelection()
            select(indexPath)
            var item = dataModel.data(indexPath)
            if (item){
                app.currentItem = item
                var page = stopWatch.createObject()
                activePane.push(page)
            
            }
        }
        
        accessibility.name: "StopWatches"
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        dataModel: app.sourceModel
        listItemComponents: [
            ListItemComponent {
                type: "item"
                ListItemStopWatch{}
            }
        ]
        onSelectionChanged: {
            if (selected){
                app.currentItem = dataModel.data(indexPath)
            }
        }
    }



} // end of SourcePage