import bb.cascades 1.2
import bb.system 1.2
import "../components"

Container {
    id: root
    
    background: Qt.app.whiteTheme ? Color.create("#f8f8f8") : Color.create("#262626")
    layout: DockLayout {}
    preferredHeight: 135
    horizontalAlignment: HorizontalAlignment.Fill
    verticalAlignment: VerticalAlignment.Fill
    
    Divider {
        verticalAlignment: VerticalAlignment.Bottom
    }
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        leftPadding: 15
        rightPadding: 15
        ImageButton {
            color: Qt.app.currentItem.colorAccent
            preferredHeight: 130
            preferredWidth: preferredHeight
            horizontalAlignment: HorizontalAlignment.Right
            verticalAlignment: VerticalAlignment.Center
            text: qsTr("Remove") + Retranslate.onLocaleOrLanguageChanged
            textSize: 5
            onClicked: {
                root.ListItem.view.removeLap(root.ListItem.indexPath)
            }
        }
        
        Container {
            layoutProperties: StackLayoutProperties {
                spaceQuota: 1
            }
            verticalAlignment: VerticalAlignment.Center
            MyLabel {
                horizontalAlignment: HorizontalAlignment.Center
                verticalAlignment: VerticalAlignment.Center
                topMargin: 0
                text: ListItemData.lap
                size: 15
                align: TextAlign.Center
                color: Qt.app.whiteTheme ? Color.Black : Color.White
                bottomMargin: 0
            } // end of TitleLabel
        } // end of Title container
        
        ImageButton {
            color: Qt.app.currentItem.colorAccent
            preferredHeight: 130
            preferredWidth: preferredHeight
            horizontalAlignment: HorizontalAlignment.Right
            verticalAlignment: VerticalAlignment.Center
            text: qsTr("Edit") + Retranslate.onLocaleOrLanguageChanged
            textSize: 5
            onClicked: {
                root.ListItem.view.edit(root.ListItem.indexPath)
            }
        } 
    } // end of title Container
}   