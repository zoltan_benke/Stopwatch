import bb.cascades 1.2

Label {
    id: root
    
    property alias color: textStyle.color
    property alias size: textStyle.fontSizeValue
    property alias bold: textStyle.fontWeight
    property alias align: textStyle.textAlign
    property alias family: textStyle.fontFamily
    
    attachedObjects: [
        TextStyleDefinition {
            id: textStyle
            fontSize: FontSize.PointValue
        }
    ]
    
    textStyle.base: textStyle.style
}