import bb.cascades 1.2

Page {
    id: root
    property bool active: false
    property bool destroy: false
    property alias header: root.titleBar
    default property alias contentItems: mainContainer.controls
    
    actionBarAutoHideBehavior: ActionBarAutoHideBehavior.HideOnScroll
    
    Container {
        id: mainContainer
        //background: Color.create("#f8f8f8")
        layout: DockLayout {}    
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
    }
}
