import bb.cascades 1.2


Container {
    id: root
    
    property alias text: label.text
    property variant textColor: Color.White
    property int textSize: 7
    property string color
    signal clicked();
    
    layout: DockLayout{}
    background: paint.imagePaint

    attachedObjects: [
        ImagePaintDefinition {
            id: paint
            imageSource: "asset:///images/colors/".concat(color ? color.replace("#", "") : "0098f0").concat(".png")
        }
    ]
    
    
    gestureHandlers: [
        TapHandler {
            onTapped: {
                root.clicked()
            }
        }
    ]
    
    onTouch: {
        if (event.isDown()){
            root.scaleX = 0.9
            root.scaleY = root.scaleX
        }
        else if (event.isUp()){
            root.scaleX = 1
            root.scaleY = root.scaleX
        }
        else if (event.isCancel()){
            root.scaleX = 1
            root.scaleY = root.scaleX
        }
    }
    
    Label {
        id: label
        horizontalAlignment: HorizontalAlignment.Center
        verticalAlignment: VerticalAlignment.Center
        textStyle{
            fontSize: FontSize.PointValue
            fontSizeValue: root.textSize
            color: textColor
        }
    }
        
}
