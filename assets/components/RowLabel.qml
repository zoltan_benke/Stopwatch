import bb.cascades 1.2

Container {
    id: root
    
    property alias title: titleLabel.text
    property alias value: valueLabel.text
    property variant titleColor: Color.Gray
    property variant valueColor: Color.White 
    property int fontSizeTitle: 6
    property int fontSizeValue: 7
    
    layout: StackLayout {
        orientation: sizeHelper.orientation ? LayoutOrientation.TopToBottom : LayoutOrientation.LeftToRight
    }
    Label {
        layoutProperties: StackLayoutProperties {
            spaceQuota: 1
        }
        accessibility.name: "title"
        id: titleLabel
        textStyle{
            fontSize: FontSize.PointValue
            fontSizeValue: root.fontSizeTitle
            fontWeight: FontWeight.Bold
            color: titleColor
        }
    }
    
    
    Label {
        accessibility.name: "value"
        id: valueLabel
        textStyle{
            fontSize: FontSize.PointValue
            fontSizeValue: root.fontSizeValue
            color: valueColor
        }
    }
    
}