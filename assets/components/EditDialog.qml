import bb.cascades 1.2
import bb.system 1.2

CustomDialog {
    id: root
    
    property variant lap: null
    height: 200
    lineColor: Color.create(Qt.app.currentItem.colorAccent)
    
    signal saved(variant newLap);
    
    function numericOnly (textin) {
        var m_strOut = new String (textin);
        m_strOut = m_strOut.replace(/[^\d.]/g,'');
        return m_strOut;
    }
    
    function setLap(lap){
        root.lap = lap
        root.title = lap.lap
        minutes.text = Qt.formatTime(new Date(lap.timestamp), "mm")
        seconds.text = Qt.formatTime(new Date(lap.timestamp), "ss")
        micro.text = Qt.formatTime(new Date(lap.timestamp), "zzz")
    }
    
    
    function save(){
        if (minutes.text.length && seconds.text.length && micro.text.length){
            var newLap = Qt.app.currentItem.editLap(lap, minutes.text.trim(), seconds.text.trim(), micro.text.trim())
            if (newLap){
                root.saved(newLap)
                close()
            }
        }else{
            if (!minutes.text.length){
                minutes.requestFocus()
            }else if (!seconds.text.length){
                seconds.requestFocus()
            }else if (!micro.text.length){
                micro.requestFocus()
            }
        }
    }
    
    
    onDone: {
        save()
    }
    
    onCancel: {
        close()
    }
    
    attachedObjects: [
        SystemToast {
            id: toast
            autoUpdateEnabled: true
            button.label: ""
        }
    ]
    
    
    Container {
        layout: StackLayout {
            orientation: LayoutOrientation.LeftToRight
        }
        horizontalAlignment: HorizontalAlignment.Fill
        verticalAlignment: VerticalAlignment.Fill
        topPadding: 50
        TextField {
            id: minutes
            hintText: "mm"
            maximumLength: 2
            input{
                submitKey: SubmitKey.Next
                onSubmitted: {
                    var t = text.trim()
                    if (parseInt(t) > 59){
                        toast.body = qsTr("Maximum value for minutes is 59") + Retranslate.onLocaleOrLanguageChanged
                        toast.show()
                        minutes.requestFocus()
                    }else{
                        if (text.length == 1){
                            minutes.text = "0"+text
                        }
                        seconds.requestFocus()
                    }
                }
            }
            onTextChanging: {
                minutes.text = root.numericOnly(text.trim())
            }
            inputMode: TextFieldInputMode.NumbersAndPunctuation
        } // end of minutes
        TextField {
            id: seconds
            hintText: "ss"
            maximumLength: 2
            inputMode: TextFieldInputMode.NumbersAndPunctuation
            input{
                submitKey: SubmitKey.Next
                onSubmitted: {
                    var t = text.trim()
                    if (parseInt(t) > 59){
                        toast.body = qsTr("Maximum value for seconds is 59") + Retranslate.onLocaleOrLanguageChanged
                        toast.show()
                        seconds.requestFocus()
                    }else{
                        if (text.length == 1){
                            seconds.text = "0"+text
                        }
                        micro.requestFocus()
                    }
                }
            }
            onTextChanging: {
                seconds.text = root.numericOnly(text.trim())
            }
        } // end of minutes
        TextField {
            id: micro
            hintText: "zzz"
            maximumLength: 3
            inputMode: TextFieldInputMode.NumbersAndPunctuation
            input{
                submitKey: SubmitKey.EnterKey
                onSubmitted: {
                    var t = text.trim()
                    if (text.length == 1){
                        micro.text = "00"+text
                    }else if (text.length == 2){
                        micro.text = "0"+text
                    }
                    save()
                }
            }
            onTextChanging: {
                micro.text = text.trim()
            }
        } // end of minutes
    }
}
