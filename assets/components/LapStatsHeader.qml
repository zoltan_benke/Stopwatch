import bb.cascades 1.2

Header {
    title: qsTr("Laps stats") + Retranslate.onLocaleOrLanguageChanged
    subtitle: qsTr("Total laps %1").arg(Qt.app.currentItem.totalLaps) + Retranslate.onLocaleOrLanguageChanged
}