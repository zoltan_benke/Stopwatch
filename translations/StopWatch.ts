<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0">
<defaultcodec>UTF-8</defaultcodec>
<context>
    <name>About</name>
    <message>
        <location filename="../assets/pages/About.qml" line="74"/>
        <source>Version: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/About.qml" line="79"/>
        <source>Developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/About.qml" line="106"/>
        <source>In case of any problems, please contact me</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ApplicationUI</name>
    <message>
        <location filename="../src/applicationui.cpp" line="160"/>
        <source>Stopwatch %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Cover</name>
    <message>
        <location filename="../assets/Cover.qml" line="29"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover.qml" line="31"/>
        <source>Not charging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover.qml" line="33"/>
        <source>Charging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover.qml" line="35"/>
        <source>Discharging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover.qml" line="37"/>
        <source>Full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover.qml" line="53"/>
        <source>Battery status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover.qml" line="53"/>
        <source>Active stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover.qml" line="89"/>
        <source>Best lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover.qml" line="97"/>
        <source>Worst lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover.qml" line="105"/>
        <source>Average lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover.qml" line="113"/>
        <source>Latest lap</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Cover-n</name>
    <message>
        <location filename="../assets/Cover-n.qml" line="29"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover-n.qml" line="31"/>
        <source>Not charging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover-n.qml" line="33"/>
        <source>Charging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover-n.qml" line="35"/>
        <source>Discharging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover-n.qml" line="37"/>
        <source>Full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover-n.qml" line="53"/>
        <source>Active stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover-n.qml" line="53"/>
        <source>Battery status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover-n.qml" line="90"/>
        <source>Best lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover-n.qml" line="99"/>
        <source>Worst lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover-n.qml" line="108"/>
        <source>Average lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/Cover-n.qml" line="117"/>
        <source>Latest lap</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomDialog</name>
    <message>
        <location filename="../assets/components/CustomDialog.qml" line="97"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/components/CustomDialog.qml" line="104"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomizeAction</name>
    <message>
        <location filename="../assets/actions/CustomizeAction.qml" line="8"/>
        <source>Customize</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomizeDialog</name>
    <message>
        <location filename="../assets/components/CustomizeDialog.qml" line="33"/>
        <source>name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomizePage</name>
    <message>
        <location filename="../assets/pages/CustomizePage.qml" line="22"/>
        <source></source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DefaultAction</name>
    <message>
        <location filename="../assets/actions/DefaultAction.qml" line="8"/>
        <source>Set as default</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditDialog</name>
    <message>
        <location filename="../assets/components/EditDialog.qml" line="80"/>
        <source>Maximum value for minutes is 59</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/components/EditDialog.qml" line="106"/>
        <source>Maximum value for seconds is 59</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Export</name>
    <message>
        <location filename="../src/custom/export.cpp" line="170"/>
        <source>Select location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/custom/export.cpp" line="206"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/custom/export.cpp" line="207"/>
        <location filename="../src/custom/export.cpp" line="230"/>
        <source>Total time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/custom/export.cpp" line="208"/>
        <location filename="../src/custom/export.cpp" line="234"/>
        <source>Best lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/custom/export.cpp" line="209"/>
        <location filename="../src/custom/export.cpp" line="238"/>
        <source>Worst lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/custom/export.cpp" line="210"/>
        <location filename="../src/custom/export.cpp" line="242"/>
        <source>Average lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/custom/export.cpp" line="211"/>
        <location filename="../src/custom/export.cpp" line="246"/>
        <source>Latest lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/custom/export.cpp" line="213"/>
        <source>Total laps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/custom/export.cpp" line="251"/>
        <source>Laps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/actions/Export.qml" line="10"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LapStatisticsLandscape</name>
    <message>
        <location filename="../assets/components/LapStatisticsLandscape.qml" line="25"/>
        <source>Best lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/components/LapStatisticsLandscape.qml" line="49"/>
        <source>Worst lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/components/LapStatisticsLandscape.qml" line="75"/>
        <source>Average lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/components/LapStatisticsLandscape.qml" line="100"/>
        <source>Latest lap</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LapStatisticsPortrait</name>
    <message>
        <location filename="../assets/720x720/components/LapStatisticsPortrait.qml" line="22"/>
        <location filename="../assets/components/LapStatisticsPortrait.qml" line="22"/>
        <source>Best lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/components/LapStatisticsPortrait.qml" line="47"/>
        <location filename="../assets/components/LapStatisticsPortrait.qml" line="47"/>
        <source>Worst lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/components/LapStatisticsPortrait.qml" line="74"/>
        <location filename="../assets/components/LapStatisticsPortrait.qml" line="74"/>
        <source>Average lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/components/LapStatisticsPortrait.qml" line="100"/>
        <location filename="../assets/components/LapStatisticsPortrait.qml" line="100"/>
        <source>Latest lap</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LapStatsHeader</name>
    <message>
        <location filename="../assets/components/LapStatsHeader.qml" line="4"/>
        <source>Laps stats</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/components/LapStatsHeader.qml" line="5"/>
        <source>Total laps %1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Laps</name>
    <message>
        <location filename="../assets/720x720/pages/Laps.qml" line="114"/>
        <source>No laps</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListItemLaps</name>
    <message>
        <location filename="../assets/720x720/items/ListItemLaps.qml" line="33"/>
        <location filename="../assets/items/ListItemLaps.qml" line="32"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/items/ListItemLaps.qml" line="63"/>
        <location filename="../assets/items/ListItemLaps.qml" line="62"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListItemStopWatch</name>
    <message>
        <location filename="../assets/720x720/items/ListItemStopWatch.qml" line="187"/>
        <location filename="../assets/items/ListItemStopWatch.qml" line="191"/>
        <source>Lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/items/ListItemStopWatch.qml" line="101"/>
        <location filename="../assets/items/ListItemStopWatch.qml" line="105"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/items/ListItemStopWatch.qml" line="101"/>
        <location filename="../assets/items/ListItemStopWatch.qml" line="105"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/items/ListItemStopWatch.qml" line="165"/>
        <location filename="../assets/items/ListItemStopWatch.qml" line="170"/>
        <source>Latest lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/items/ListItemStopWatch.qml" line="173"/>
        <location filename="../assets/items/ListItemStopWatch.qml" line="178"/>
        <source>Total laps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/items/ListItemStopWatch.qml" line="101"/>
        <location filename="../assets/items/ListItemStopWatch.qml" line="105"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/items/ListItemStopWatch.qml" line="187"/>
        <location filename="../assets/items/ListItemStopWatch.qml" line="191"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewTimer</name>
    <message>
        <location filename="../assets/delegates/NewTimer.qml" line="25"/>
        <source>Set time</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RegistrationHandler</name>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="37"/>
        <source>Please wait while the application connects to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="116"/>
        <source>Application connected to BBM.  Press Continue.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="128"/>
        <source>Disconnected by RIM. RIM is preventing this application from connecting to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="134"/>
        <source>Disconnected. Go to Settings -&gt; Security and Privacy -&gt; Application Permissions and connect this application to BBM.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="142"/>
        <source>Invalid UUID. Report this error to the vendor.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="148"/>
        <source>Too many applications are connected to BBM. Uninstall one or more applications and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="156"/>
        <source>Cannot connect to BBM. Download this application from AppWorld to keep using it.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="162"/>
        <source>Check your Internet connection and try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="169"/>
        <source>Connecting to BBM. Please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="174"/>
        <source>Determining the status. Please wait.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/platform/RegistrationHandler.cpp" line="184"/>
        <source>Would you like to connect the application to BBM?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveFile</name>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="82"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="104"/>
        <source>File format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="123"/>
        <source>CSV delimiter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="126"/>
        <source>tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="130"/>
        <source>comma</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="134"/>
        <source>semicolon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="144"/>
        <source>Filename format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="147"/>
        <source>date-time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="151"/>
        <source>date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="155"/>
        <source>time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="165"/>
        <source>Export time format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="228"/>
        <source>Active stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="232"/>
        <source>All stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="176"/>
        <source>Custom format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="55"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="193"/>
        <source>Enter custom time format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/SaveFile.qml" line="219"/>
        <source>Time format

m - (0 to 59)
mm - (00 to 59)
s - (0 to 59)
ss - (00 to 59)
z - (0 to 999)
zzz - (000 to 999)

example:
m,ss-zzz
output:
2,59-987</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaveToFile</name>
    <message>
        <location filename="../assets/actions/SaveToFile.qml" line="10"/>
        <source>Save to file</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SendEmail</name>
    <message>
        <location filename="../assets/actions/SendEmail.qml" line="7"/>
        <source>Send e-mail</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../assets/pages/Settings.qml" line="36"/>
        <source>To apply changes restart application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="53"/>
        <source>General settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="71"/>
        <source>Show customize dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="77"/>
        <source>Shows customize dialog after you add stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="99"/>
        <source>Active frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="109"/>
        <source>Frame</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="112"/>
        <source>Battery status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="117"/>
        <source>Active stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="127"/>
        <source>Application theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="137"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="140"/>
        <source>Bright</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="145"/>
        <source>Dark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/Settings.qml" line="156"/>
        <source>Application color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Share</name>
    <message>
        <location filename="../assets/actions/Share.qml" line="10"/>
        <source>Share</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShareCsv</name>
    <message>
        <location filename="../assets/actions/ShareCsv.qml" line="10"/>
        <source>Share CSV</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShareTxt</name>
    <message>
        <location filename="../assets/actions/ShareTxt.qml" line="10"/>
        <source>Share TEXT</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StopWatch</name>
    <message>
        <location filename="../assets/720x720/pages/StopWatch.qml" line="33"/>
        <location filename="../assets/pages/StopWatch.qml" line="37"/>
        <source>Stop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/pages/StopWatch.qml" line="33"/>
        <location filename="../assets/pages/StopWatch.qml" line="37"/>
        <source>Start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/pages/StopWatch.qml" line="48"/>
        <location filename="../assets/pages/StopWatch.qml" line="49"/>
        <source>Lap</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/720x720/pages/StopWatch.qml" line="48"/>
        <location filename="../assets/pages/StopWatch.qml" line="49"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/pages/StopWatch.qml" line="209"/>
        <source>No laps</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>StopWatches</name>
    <message>
        <location filename="../assets/tabs/StopWatches.qml" line="13"/>
        <source>Stopwatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/tabs/StopWatches.qml" line="26"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/tabs/StopWatches.qml" line="66"/>
        <source>To add stopwatch use the add button below</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TimerDialog</name>
    <message>
        <location filename="../assets/components/TimerDialog.qml" line="114"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/components/TimerDialog.qml" line="122"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Timers</name>
    <message>
        <location filename="../assets/tabs/Timers.qml" line="19"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../assets/main.qml" line="43"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="51"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="60"/>
        <source>Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../assets/main.qml" line="70"/>
        <source>Tell a Friend</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
