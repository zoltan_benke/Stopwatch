#ifndef STOPWATCHITEM_H
#define STOPWATCHITEM_H

#include <QObject>
#include <QDateTime>
#include <QTimer>
#ifdef QT_DEBUG
#include <QDebug>
#endif

class StopWatchItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int id READ id WRITE setId NOTIFY dataChanged)
    Q_PROPERTY(QString name READ name WRITE setName NOTIFY dataChanged)
    Q_PROPERTY(QString title READ title NOTIFY dataChanged)
    Q_PROPERTY(bool running READ running NOTIFY dataChanged)
    Q_PROPERTY(QVariant latestLap READ latestLap NOTIFY lapsChanged)
    Q_PROPERTY(int totalLaps READ totalLaps NOTIFY lapsChanged)
    Q_PROPERTY(QVariantList laps READ laps NOTIFY lapsChanged)
    Q_PROPERTY(QString colorAccent READ colorAccent WRITE setColorAccent NOTIFY dataChanged)
    Q_PROPERTY(int bestLap READ bestLap NOTIFY lapsChanged)
    Q_PROPERTY(int worstLap READ worstLap NOTIFY lapsChanged)
    Q_PROPERTY(int averageLap READ averageLap NOTIFY lapsChanged)
    Q_PROPERTY(QString eTitle READ eTitle NOTIFY dataChanged)
    Q_PROPERTY(bool edited READ edited NOTIFY dataChanged)
public:
    explicit StopWatchItem(QObject *parent = 0);
    virtual ~StopWatchItem();


    Q_INVOKABLE
    void start();
    Q_INVOKABLE
    void stop();
    Q_INVOKABLE
    void lap();
    Q_INVOKABLE
    void reset();
    Q_INVOKABLE
    bool removeLap(const QVariant &lap);
    Q_INVOKABLE
    QVariant editLap(const QVariant &lap, const int &mm, const int &ss, const int &ms);

signals:
    void dataChanged();
    void lapsChanged(QVariantList laps);

    public slots:
    inline int id() const { return _id; }
    inline void setId(const int &value){
        if (_id != value){
            _id = value;
            emit dataChanged();
        }
    }
    inline QString name() const { return _name; }
    inline void setName(const QString &value){
        if (_name != value){
            _name = value;
            emit dataChanged();
        }
    }
    inline QString title() const { return sec()+ms(); }
    inline QString eTitle() const { return eSec()+eMs(); }
    inline bool edited() const { return _edited && title() != eTitle(); }
    inline bool running() const { return _running; }
    inline QVariantMap latestLap() const { return _laps.size() ? _laps.last().toMap() : _latestLap; }
    inline int totalLaps() const { return _laps.size(); }
    QVariantList laps() { return _laps; }
    inline QString colorAccent() const { return _color; }
    inline void setColorAccent(const QString &value){
        if (_color != value){
            _color = value;
            emit dataChanged();
        }
    }
    inline int bestLap(){
        if (_laps.size()){
            QList<int> temp;
            foreach (const QVariant &item, _laps) {
                temp << item.toMap().value("timestamp").toInt();
            }
            qSort(temp.begin(), temp.end(), qLess<int>());
            return temp.first();
        }
        return 0;
    }
    inline int worstLap(){
        if (_laps.size()){
            QList<int> temp;
            foreach (const QVariant &item, _laps) {
                temp << item.toMap().value("timestamp").toInt();
            }
            qSort(temp.begin(), temp.end(), qGreater<int>());
            return temp.first();
        }
        return 0;
    }
    inline int averageLap(){
        if (_laps.size()){
            int sum = 0;
            foreach (const QVariant &item, _laps) {
                sum += item.toMap().value("timestamp").toInt();
            }
            int avg = sum/_laps.size();
            return avg;
        }
        return 0;
    }

private slots:
    inline QString sec() const { return QDateTime::fromMSecsSinceEpoch(_sTime).toString("mm:ss"); }
    inline QString ms() const { return QDateTime::fromMSecsSinceEpoch(_mTime).toString(":zzz"); }
    inline QString eSec() const { return QDateTime::fromMSecsSinceEpoch(_sTime+_modulo).toString("mm:ss"); }
    inline QString eMs() const { return QDateTime::fromMSecsSinceEpoch(_mTime+_modulo).toString(":zzz"); }
    void timerSecSlot();
    void timerMicroSlot();
    QVariantMap getLap(const int &timestamp){
        QVariantMap map;
        map.insert("timestamp", timestamp);
        map.insert("lap", QDateTime::fromMSecsSinceEpoch(timestamp).toString("mm:ss:zzz"));
        map.insert("edited", false);
        map.insert("editedTime", 0);
        map.insert("originalTime", 0);
        return map;
    }

    private:
    Q_DISABLE_COPY(StopWatchItem)
    int _id;
    QString _name;
    bool _running;
    QVariantMap _latestLap;
    QVariantList _laps;
    QString _color;

protected:
    QTimer* _timerSec;
    QTimer* _timerMicro;
    QDateTime _startTime;
    QDateTime _latest;
    int _sTime;
    int _mTime;
    qint64 _startTimeMsec;
    qint64 _timeElapsed;
    QString _eTitle;
    qint64 _modulo;
    bool _edited;
};

#endif // STOPWATCHITEM_H
