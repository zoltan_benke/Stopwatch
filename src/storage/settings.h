/*
 * settings.h
 *
 *  Created on: 27.7.2014
 *      Author: benecore
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <QtCore/QSettings>


const char *const GENERAL = "GENERAL";
const char *const APPEARANCE = "APPEARANCE";


class Settings: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariant color READ color WRITE setColor NOTIFY settingsChanged)
    Q_PROPERTY(bool showCustomizeDialog READ showCustomizeDialog WRITE setShowCustomizeDialog NOTIFY settingsChanged)
    Q_PROPERTY(bool frameBattery READ frameBattery WRITE setFrameBattery NOTIFY settingsChanged)
    // EXPORT SETTINGS
    Q_PROPERTY(int fileFormat READ fileFormat WRITE setFileFormat NOTIFY settingsChanged)
    Q_PROPERTY(int fileNameFormat READ fileNameFormat WRITE setFileNameFormat NOTIFY settingsChanged)
    Q_PROPERTY(int csvDelimeter READ csvDelimeter WRITE setCsvDelimeter NOTIFY settingsChanged)
    Q_PROPERTY(int timeFormat READ timeFormat WRITE setTimeFormat NOTIFY settingsChanged)
    Q_PROPERTY(QString timeFormatString READ timeFormatString WRITE setTimeFormatString NOTIFY settingsChanged)
public:
    Settings(QObject *parent = 0);
    virtual ~Settings();


    static Settings *instance();
    static void destroy();


signals:
    void settingsChanged();



public slots:
    inline QVariant color() const { return _color; }
    inline void setColor(const QVariant &value){
        if (_color != value){
            _color = value;
            s->setValue("color", _color);
            emit settingsChanged();
        }
    }
    inline bool showCustomizeDialog() const { return _showCustomizeDialog; }
    inline void setShowCustomizeDialog(const bool &value){
        if (_showCustomizeDialog != value){
            _showCustomizeDialog = value;
            s->setValue("showCustomizeDialog", _showCustomizeDialog);
            emit settingsChanged();
        }
    }
    inline bool frameBattery() const { return _frameBattery; }
    inline void setFrameBattery(const bool &value){
        if (_frameBattery != value){
            _frameBattery = value;
            s->setValue("frameBattery", _frameBattery);
            emit settingsChanged();
        }
    }
    // EXPORT SETTINGS
    inline int fileFormat() const { return _fileFormat; }
    inline void setFileFormat(const int &value){
        if (_fileFormat != value){
            _fileFormat = value;
            s->setValue("fileFormat", _fileFormat);
            emit settingsChanged();
        }
    }
    inline int fileNameFormat() const { return _fileNameFormat; }
    inline void setFileNameFormat(const int &value){
        if (_fileNameFormat != value){
            _fileNameFormat = value;
            s->setValue("fileNameFormat", _fileNameFormat);
            emit settingsChanged();
        }
    }
    inline int csvDelimeter() const { return _csvDelimeter; }
    inline void setCsvDelimeter(const int &value){
        if (_csvDelimeter != value){
            _csvDelimeter = value;
            s->setValue("csvDelimeter", _csvDelimeter);
            emit settingsChanged();
        }
    }
    inline int timeFormat() const { return _timeFormat; }
    inline void setTimeFormat(const int &value){
        if (_timeFormat != value){
            _timeFormat = value;
            s->setValue("timeFormat", _timeFormat);
            emit settingsChanged();
        }
    }
    inline QString timeFormatString() const { return _timeFormatString; }
    inline void setTimeFormatString(const QString &value){
        if (_timeFormatString != value){
            _timeFormatString = value;
            s->setValue("timeFormatString", _timeFormatString);
            emit settingsChanged();
        }
    }



private:
    Q_DISABLE_COPY(Settings)
    static Settings *_instance;
    QSettings *s;
    //
    QVariant _color;
    bool _showCustomizeDialog;
    bool _frameBattery;
    // Export settings
    int _fileFormat;
    int _fileNameFormat;
    int _csvDelimeter;
    int _timeFormat;
    QString _timeFormatString;

};

#endif /* SETTINGS_H_ */
