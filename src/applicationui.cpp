/*
 * Copyright (c) 2011-2014 BlackBerry Limited.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "applicationui.hpp"

#include <bb/cascades/Application>
#include <bb/cascades/QmlDocument>
#include <bb/cascades/AbstractPane>
#include <bb/cascades/LocaleHandler>


using namespace bb::cascades;
using namespace bb::platform;

ApplicationUI::ApplicationUI() :
                                                                                QObject(),
                                                                                _sourceModel(0),
                                                                                _currentItem(0),
                                                                                qmlCover(0),
                                                                                coverContainer(0),
                                                                                sceneCover(0)
{

#ifdef FREE_VERSION
    _freeVersion = true;
#elif !defined(FREE_VERSION)
    _freeVersion = false;
#endif
    QTextCodec::setCodecForCStrings(QTextCodec::codecForName("utf-8"));
    baseItem = new StopWatchItem(this);
    setCurrentItem(baseItem);
    /** Registration handler **/
    const QString uuid(QLatin1String("e0c9e5b0-70f4-4fb1-aeb2-612a342b4b6c"));
    _registrationHandler = new RegistrationHandler(uuid);
    _inviteDownload = new InviteToDownload(_registrationHandler->context());

    platformInfo = new PlatformInfo(this);
    qDebug() << "OS VERSION" << platformInfo->osVersion();
    _sourceModel = new GroupDataModel(this);
    _sourceModel->setGrouping(ItemGrouping::None);
    _sourceModel->setSortedAscending(true);
    _sourceModel->setSortingKeys(QStringList() << "id");

    s = Settings::instance();
    _sizeHelper = SizeHelper::instance();
    // prepare the localization
    m_pTranslator = new QTranslator(this);
    m_pLocaleHandler = new LocaleHandler(this);

    //connect(Application::instance(), SIGNAL(thumbnail()), this, SLOT(thumbnail()));

    bool res = QObject::connect(m_pLocaleHandler, SIGNAL(systemLanguageChanged()), this, SLOT(onSystemLanguageChanged()));
    // This is only available in Debug builds
    Q_ASSERT(res);
    // Since the variable is not used in the app, this is added to avoid a
    // compiler warning
    Q_UNUSED(res);

    // initial load
    onSystemLanguageChanged();

    qmlRegisterType<StopWatchItem>("bb.cascades", 1, 2, "StopWatchItem");
    qmlRegisterType<Invoker>("bb.cascades", 1, 2, "Invoker");
    // Create scene document from main.qml asset, the parent is set
    // to ensure the document gets destroyed properly at shut down.
    QmlDocument *qml = QmlDocument::create("asset:///main.qml").parent(this)
                                                                                    .property("app", this)
                                                                                    .property("sourceModel", _sourceModel)
                                                                                    .property("settings", s)
                                                                                    .property("sizeHelper", _sizeHelper)
                                                                                    .property("appTheme", ThemeSettings::instance())
                                                                                    .property("Export", Export::instance())
                                                                                    .property("BBM", _registrationHandler)
                                                                                    .property("Invite", _inviteDownload);;

    // Create root object for the UI
    AbstractPane *root = qml->createRootObject<AbstractPane>();

    qmlCover = QmlDocument::create(_sizeHelper->nType() ? "asset:///Cover-n.qml" : "asset:///Cover.qml").parent(this)
                        .property("app", this)
                        .property("sourceModel", _sourceModel)
                        .property("settings", s)
                        .property("sizeHelper", _sizeHelper);

        if (!qmlCover->hasErrors()) {
            // Create the QML Container from using the QMLDocument.
            coverContainer = qmlCover->createRootObject<Container>();

            // Create a SceneCover and set the application cover
            sceneCover = SceneCover::create().content(coverContainer);
            Application::instance()->setCover(sceneCover);
        }

    // Set created root object as the application scene
    Application::instance()->setScene(root);
#ifdef QT_DEBUG
    new QmlBeam(this);
#endif
}

ApplicationUI::~ApplicationUI()
{
    delete _sourceModel;
    delete s;
    delete _sizeHelper;
    ThemeSettings::destroy();
    Export::destroy();
    delete platformInfo;
}

void ApplicationUI::thumbnail()
{
#ifdef QT_DEBUG
    qDebug() << Q_FUNC_INFO;
#endif
    if (qmlCover){
        delete qmlCover;
        qmlCover = 0;
    }
    if (coverContainer){
        delete coverContainer;
        coverContainer = 0;
    }
    if (sceneCover){
        delete sceneCover;
        sceneCover = 0;
    }
    qmlCover = QmlDocument::create("asset:///Cover.qml").parent(this)
                    .property("app", this)
                    .property("sourceModel", _sourceModel)
                    .property("settings", s)
                    .property("sizeHelper", _sizeHelper);

    if (!qmlCover->hasErrors()) {
        // Create the QML Container from using the QMLDocument.
        coverContainer = qmlCover->createRootObject<Container>();

        // Create a SceneCover and set the application cover
        sceneCover = SceneCover::create().content(coverContainer);
        Application::instance()->setCover(sceneCover);
    }
}

void ApplicationUI::append()
{
    StopWatchItem *item = new StopWatchItem(this);
    item->setName(tr("Stopwatch %1").arg(!_sourceModel->size() ? QString::number(1) : QString::number(_sourceModel->size()+1)));
    item->setId(!_sourceModel->size() ? 1 : _sourceModel->size()+1);
    _sourceModel->insert(item);
    if (_sourceModel->size() == 1){
        setCurrentItem(item);
    }
    emit countChanged();
}

void ApplicationUI::removeAll()
{
    _sourceModel->clear();
    setCurrentItem(baseItem);
    emit countChanged();
}


void ApplicationUI::removeItem(const QVariantList &indexPath)
{
    _sourceModel->removeAt(indexPath);
    if (!_sourceModel->size())
        setCurrentItem(baseItem);
    emit countChanged();
}

void ApplicationUI::startItem(const QVariantList& indexPath)
{
    QObject *obj = _sourceModel->data(indexPath).value<QObject*>();
    StopWatchItem *item = qobject_cast<StopWatchItem*>(obj);
    if (item){
        qDebug() << "START";
        setCurrentItem(item);
        item->start();
    }
}

void ApplicationUI::stopItem(const QVariantList& indexPath)
{
    QObject *obj = _sourceModel->data(indexPath).value<QObject*>();
    StopWatchItem *item = qobject_cast<StopWatchItem*>(obj);
    if (item){
        qDebug() << "STOP";
        item->stop();
    }
}

void ApplicationUI::lapItem(const QVariantList& indexPath)
{
    QObject *obj = _sourceModel->data(indexPath).value<QObject*>();
    StopWatchItem *item = qobject_cast<StopWatchItem*>(obj);
    if (item){
        qDebug() << "LAP";
        item->lap();
    }
}

void ApplicationUI::resetItem(const QVariantList& indexPath)
{
    QObject *obj = _sourceModel->data(indexPath).value<QObject*>();
    StopWatchItem *item = qobject_cast<StopWatchItem*>(obj);
    if (item){
        qDebug() << "RESET";
        item->reset();
    }
}

void ApplicationUI::removeLap(const QVariantList& indexPath, const QVariant& lap)
{
    QObject *obj = _sourceModel->data(indexPath).value<QObject*>();
    StopWatchItem *item = qobject_cast<StopWatchItem*>(obj);
    if (item){
        qDebug() << "REMOVE LAP";
        item->removeLap(lap);
    }
}

void ApplicationUI::onSystemLanguageChanged()
{
    QCoreApplication::instance()->removeTranslator(m_pTranslator);
    // Initiate, load and install the application translation files.
    QString locale_string = QLocale().name();
    QString file_name = QString("StopWatch_%1").arg(locale_string);
    if (m_pTranslator->load(file_name, "app/native/qm")) {
        QCoreApplication::instance()->installTranslator(m_pTranslator);
    }
}


