#ifndef EXPORT_H
#define EXPORT_H

#include <QObject>
#include <QList>
#include <QFile>
#ifdef BB10
#include <bb/cascades/GroupDataModel>
#include "invoker.h"
#include <bb/cascades/pickers/FilePicker>
#include <bb/PpsObject>
#include <bb/system/InvokeManager>
#include <bb/system/InvokeRequest>
#include <bb/cascades/Invocation>
#include <bb/cascades/InvokeQuery>
#endif
#ifdef QT_DEBUG
#include <QDebug>
#endif

#include "../items/stopwatchitem.h"

const char *const LF = "\n";
const char *const CR = "\r";
const char *const CRLF = "\r\n";
const char *const APPOSTROF = "\"";

class Export : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString timeFormat READ timeFormat WRITE setTimeFormat NOTIFY timeFormatChanged)
    Q_PROPERTY(QString result READ result WRITE setResult NOTIFY resultChanged)
public:
    explicit Export(QObject *parent = 0);
    ~Export();

    static Export *instance();
    static void destroy();


signals:
    void start(bool activeStopwatch);
    void timeFormatChanged();
    void resultChanged();
    void fileSaved(const QString filePath);

public slots:
    inline void iniciate(const bool &value = true){
        emit start(value);
    }

    QString result() const { return _result; }
    void setResult(const QString &value){
        _result = value;
        emit resultChanged();
    }

    void exportAllToCsv(QList<QObject*> &objects, const bool &escape = false, const QString &separator = "\t");
    void exportAllToCsv(QList<StopWatchItem*> &items, const bool &escape = false, const QString &separator = "\t");
#ifdef BB10
    void exportAllToCsv(bb::cascades::GroupDataModel *model, const bool &escape = false, const QString &separator = "\t");
    void exportAllToTxt(bb::cascades::GroupDataModel *model);
    void saveToFile(const QString &fileName);
    void invokeEmail(const QString &fileName, const bool &attachment = false);
    void shareFile(const QString &fileName);
#endif
    void exportItemToCsv(StopWatchItem *item, const bool &escape = false, const QString &separator = "\t");
    void exportItemToTxt(StopWatchItem *item);



private slots:
    QString timeFormat() const { return _timeFormat; }
    inline void setTimeFormat(const QString &value){
        if (_timeFormat != value){
            _timeFormat = value;
            emit timeFormatChanged();
        }
    }
    QString toTime(const int &timestamp);
    QString escapeString(const QString &value, const bool &escape);
#ifdef BB10
    void fileSelected(const QStringList& fileList);
    void invocationFinished();
    void onArmed();
#endif

private:
    Q_DISABLE_COPY(Export)
    static Export *_instance;
    QString _result;
    QString _timeFormat;
    QString _tempFile;
#ifdef BB10
    Invoker *invoker;
    bb::system::InvokeManager *invokeManager;
    bb::system::InvokeRequest *request;
    bb::cascades::Invocation *invocation;
#endif
};

#endif // EXPORT_H
