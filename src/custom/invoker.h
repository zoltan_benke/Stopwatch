/*
 * Invoker.hpp
 *
 *  Created on: Oct 28, 2013
 *      Author: Martin R. Green
 *     Licence: CC By 3.0 (http://creativecommons.org/licenses/by/3.0)
 *
 */

#ifndef INVOKER_HPP_
#define INVOKER_HPP_

#include <bb/cascades/BaseObject>
#include <bb/system/InvokeManager>
#include <bb/system/InvokeReply>
#include <bb/system/InvokeTargetReply>
#include <bb/system/InvokeRequest>

using namespace bb::system;

class Invoker: public bb::cascades::BaseObject
{
    Q_OBJECT

    InvokeRequest* _request;

    InvokeReplyError::Type _lastError;
    Q_PROPERTY(InvokeReplyError::Type lastError READ lastError)
    Q_PROPERTY(QString lastErrorText READ lastErrorText)
    Q_PROPERTY(bool valid READ valid)

    // ---InvokeRequest properties
    Q_PROPERTY(QString action READ action WRITE setAction)
    Q_PROPERTY(QUrl uri READ uri WRITE setUri)
    Q_PROPERTY(QString target READ target WRITE setTarget)
    Q_PROPERTY(QByteArray data READ data WRITE setData)
    Q_PROPERTY(FileTransferMode::Type fileTransferMode READ fileTransferMode WRITE setFileTransferMode)
    Q_PROPERTY(int listId READ listId WRITE setListId)
    Q_PROPERTY(QVariantMap metaData READ metaData WRITE setMetaData)
    Q_PROPERTY(QString mimeType READ mimeType WRITE setMimeType)
    Q_PROPERTY(SecurityPerimeter::Type perimeter READ perimeter WRITE setPerimeter)
    Q_PROPERTY(InvokeTarget::Types targetTypes READ targetTypes WRITE setTargetTypes)

private Q_SLOTS:
    void invokeReplyFinishedHandler();

Q_SIGNALS:
    void invocationFinished();
    void invocationFailed();

public:
    Invoker( QObject* parent = NULL );
    ~Invoker();

    Q_INVOKABLE void invoke();

    // ---Can't make a slot Q_INVOKABLE so need a separate slot
    Q_SLOT void invokeSlot();

    // ---Accessors
    bool valid();
    InvokeReplyError::Type lastError();
    QString lastErrorText();

    QString action();
    Q_SLOT QString setAction( const QString& action );

    QUrl uri() const;
    Q_SLOT QUrl setUri ( const QUrl& uri ) const;
    Q_SLOT QString setUri ( const QString& uri ) const;
    Q_SLOT char* setUri ( const char* uri ) const;

    QString target() const;
    Q_SLOT QString setTarget( const QString& target ) const;

    QByteArray data();
    Q_SLOT QByteArray setData(const QByteArray& data );

    FileTransferMode::Type fileTransferMode();
    Q_SLOT FileTransferMode::Type setFileTransferMode( FileTransferMode::Type fileTransferMode );

    int listId();
    Q_SLOT int setListId( int listId );

    QVariantMap metaData() const;
    Q_SLOT QVariantMap setMetaData( const QVariantMap& metaData ) const;

    QString mimeType() const;
    Q_SLOT QString setMimeType( const QString& mimeType ) const;

    SecurityPerimeter::Type perimeter();
    Q_SLOT SecurityPerimeter::Type setPerimeter( SecurityPerimeter::Type perimeter );

    InvokeTarget::Types targetTypes();
    Q_SLOT InvokeTarget::Types setTargetTypes( InvokeTarget::Types targetTypes );

    // ---Builder template
    template<typename BuilderType, typename BuiltType>
    class TBuilder: public BaseObject::TBuilder<BuilderType, BuiltType>
    {
    protected:
        TBuilder( BuiltType* node ) :
            BaseObject::TBuilder<BuilderType, BuiltType>( node ) {
        }
    public:

        // ---Builder parameters
        BuilderType& parent( BaseObject* parent ) {
            this->instance().setParent( parent );
            return this->builder();
        }

        BuilderType& action( const QString& action ) {
            this->instance().setAction( action );
            return this->builder();
        }

        BuilderType& uri( const QUrl& uri ) {
            this->instance().setUri( uri );
            return this->builder();
        }

        BuilderType& uri( const QString& uri ) {
            this->instance().setUri( uri );
            return this->builder();
        }

        BuilderType& uri( const char* action ) {
            this->instance().setUri( action );
            return this->builder();
        }

        BuilderType& target( const QString& target ) {
            this->instance().setTarget( target );
            return this->builder();
        }

        BuilderType& data( const QByteArray& data ) {
            this->instance().setData( data );
            return this->builder();
        }

        BuilderType& fileTransferMode( FileTransferMode::Type fileTransferMode ) {
            this->instance().setFileTransferMode( fileTransferMode );
            return this->builder();
        }

        BuilderType& listId( int listId ) {
            this->instance().setListId( listId );
            return this->builder();
        }

        BuilderType& metaData( const QVariantMap& metaData ) {
            this->instance().setMetaData( metaData );
            return this->builder();
        }

        BuilderType& mimeType( const QString& mimeType ) {
            this->instance().setMimeType( mimeType );
            return this->builder();
        }

        BuilderType& perimeter( SecurityPerimeter::Type perimeter ) {
            this->instance().setPerimeter( perimeter );
            return this->builder();
        }

        BuilderType& targetTypes( InvokeTarget::Types targetTypes ) {
            this->instance().setTargetTypes( targetTypes );
            return this->builder();
        }
    };

    class Builder: public TBuilder<Builder, Invoker>
    {
    public:
        Builder() : TBuilder<Builder, Invoker>( new Invoker() ) {
        }
    };

    static Builder create() {
        return Builder();
    }
};

#endif /* INVOKER_HPP_ */
