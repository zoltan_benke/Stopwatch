/*
 * themesettings.h
 *
 *  Created on: 4.1.2014
 *      Author: Benecore
 */

#ifndef THEMESETTINGS_H_
#define THEMESETTINGS_H_

#include <QObject>
#include <QSettings>
#include <bb/device/HardwareInfo>


using namespace bb::device;

class ThemeSettings : public QObject {
	Q_OBJECT
public:
	ThemeSettings();
	virtual ~ThemeSettings();

	static ThemeSettings *instance();
	static void destroy();
	/**
	 * This Invokable function gets a value from the QSettings,
	 * if that value does not exist in the QSettings database, the default value is returned.
	 *
	 * @param objectName Index path to the item
	 * @param defaultValue Used to create the data in the database when adding
	 * @return If the objectName exists, the value of the QSettings object is returned.
	 *         If the objectName doesn't exist, the default value is returned.
	 */
	Q_INVOKABLE
	QString getThemeString(const QString &objectName, const QString &defaultValue = "");

	/**
	 * This function sets a value in the QSettings database. This function should to be called
	 * when a data value has been updated from QML
	 *
	 * @param objectName Index path to the item
	 * @param inputValue new value to the QSettings database
	 */
	Q_INVOKABLE
	void setThemeString(const QString &objectName, const QString &inputValue);

signals:
	void themeSaved();

private:
	static ThemeSettings *_instance;
	QSettings settings;
	QString _themeString;
	HardwareInfo *info;
};

#endif /* THEMESETTINGS_H_ */
